<?php

/**
 * Description of VibeContact
 *
 * @author Wilson Mazaiwana - 11/11/16 10:30
 */
class VibeContact {
    //put your code here
    
    private $validPhoneNumber;
    
    public function __construct($phoneNumber,$countryCode) {
        $result = $this->checkPhoneNumber($phoneNumber, $countryCode);
        if($result===false){
            throw new Exception("Invalid Mobile Number");
        }else{
            $this->validPhoneNumber = $result;
        }
    }
    
    private function checkPhoneNumber($number, $countryCode){
        
        $number = strval($number);
        $number = trim($number);
        if(strlen($number)>=16 || strlen($number) < 6){
        //wrong number, return false
        return FALSE;
        }
        if(substr($number,0,1)=='+'){
            if(!ctype_digit(substr($number,1))){
                    return FALSE;
            }else{
                    $number=str_replace("+","",$number);
            }
        }

        if(!ctype_digit($number)){
            return FALSE;//it's a recipient number, has to be all digits
        }
        
        if(substr($number,0,1)=='0'){
            if(substr($number,0,2)=='00'){
                if(substr($number,0,3)=='000'){
                    return FALSE;
                }
                $number=preg_replace('/00/','',$number,1);
                return $number;
            }else{
                $number=preg_replace('/0/',$countryCode,$number,1);
                return $number;
            }
        }else{
            //number doesn't start with 0 or 00 or +(0(0))... the perfect number
            return $number;
        }
    }
    
    public function getPhoneNumber(){
        return $this->validPhoneNumber;
    }
    
}



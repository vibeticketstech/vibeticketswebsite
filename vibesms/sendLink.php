<?php
header('Access-Control-Allow-Origin: *'); 
include("./SMSJuice-API/Objects/Message.php");
include("./SMSJuice-API/Objects/ContactGroup.php");
include("./SMSJuice-API/TextMessagingService.php");

include("./VibeContact.php");
include("./config.php");
use SMSJuiceAPI\TextMessagingService;
use SMSJuiceAPI\Objects\Message;
use SMSJuiceAPI\Objects\ContactGroup;


if(isset($_POST['phoneNumber'])){
    try{
        $contact = new VibeContact($_POST['phoneNumber'], UK_CODE);

        
        //just for you Johnny, so that you can repeatedly test
        // without worrying about being blocked from sending a message to your phone again.
        //What this does is removes the number in question from the groups which contains 
        //the number so that you can repeatedly test messages. Please change "TESTING_MODE"
        // in the config file to false to resume normal function
        if(TESTING_MODE === true){
            TextMessagingService::deleteContactFromGroup(KEY, SECRET, LINK_CONTACTS, $contact->getPhoneNumber());
        }
        
        
        //check if contact exists in the groups already
        $linkcontacts = TextMessagingService::getAllGroupContacts(KEY, SECRET, LINK_CONTACTS);
        $contactslistArray = json_decode($linkcontacts, true);
        
        
        //if group doesn't exist, create one.
        if(!isset($contactslistArray) || $contactslistArray===NULL){
            $newContactGroup = new ContactGroup(KEY, SECRET, LINK_CONTACTS);
            TextMessagingService::addGroup($newContactGroup);
            //get created group
            $linkcontacts = TextMessagingService::getAllGroupContacts(KEY, SECRET, LINK_CONTACTS);
            $contactslistArray = json_decode($linkcontacts, true);
        }
        
        
        if(findNumberInContactGroup($contactslistArray['contacts'], $contact->getPhoneNumber()) === false){
            //Number is not in contacts group,
            //which means no message has been sent to it in the past,
            //so send message
            $m = new Message(KEY, SECRET);
            $m->setSender(SENDERID);
            $m->addToRecipientList($contact->getPhoneNumber());
            $m->setMessage(STORE_LINK);
            $jsonResult = TextMessagingService::sendMessage($m);
            $jsonResultArray = json_decode($jsonResult, true);
            if($jsonResultArray['submit']==="success"){
                //add the number to the contact group, only if sending was a success
                $existingContactGroup = new ContactGroup(KEY, SECRET, LINK_CONTACTS);
                $existingContactGroup->addContact($contact->getPhoneNumber());
                TextMessagingService::addContactToGroup($existingContactGroup);
                die("success");
            }else{
                die("error - {$jsonResultArray['error']}\nNumber not stored");
            }
        }else{
            die("error - number exists");
        }

    }  catch (Exception $e){
        //echo "{\"submit\":\"failed\",\"error\":\"{$e->getMessage()}\"}";
        die("error - {$e->getMessage()}");
    }
}else{
    echo "error - Invalid Form Submission";
}



function findNumberInContactGroup(Array $contactList, $contactToSearchFor){
    if(!isset($contactList) || $contactList===NULL){
        return false;
    }
    foreach($contactList as $contact){
        if($contactToSearchFor === $contact['number']){
            return true;
        }
    }
    return false;
}




?>
<?php

//************************************************//
//*******************Unit Tests*******************//
//************************************************//

include("./VibeContact.php");


//1. Test throws exception
try{$n = new VibeContact("44w490349302", 44);}catch(Exception $e){echo "44w490349302 is an " . $e->getMessage()."\n";}
try{$n = new VibeContact("000447490349302", 44);}catch(Exception $e){echo "000447490349302 is an " . $e->getMessage()."\n";}
try{$n = new VibeContact("447777777490349302", 44);}catch(Exception $e){echo "447777777490349302 is an " . $e->getMessage()."\n";}
try{$n = new VibeContact("+4479", 44);}catch(Exception $e){echo "+4479 is an " . $e->getMessage()."\n";}
try{$n = new VibeContact("", 44);}catch(Exception $e){echo " is an " . $e->getMessage()."\n";}
echo "\n";

//2. Test if it works
$n = new VibeContact("07900385586", 44);echo $n->getPhoneNumber()."\n";
$n = new VibeContact("447900385586", 44);echo $n->getPhoneNumber()."\n";
$n = new VibeContact("+447900385586", 44);echo $n->getPhoneNumber()."\n";
$n = new VibeContact("00447900385586", 44);echo $n->getPhoneNumber()."\n";
$n = new VibeContact("+00447900385586", 44);echo $n->getPhoneNumber()."\n";
<?php

/**
 * This class is used for adding new groups and adding new contacts to an existing group on your SMSJuice account
 *
 * @author Wilson Mazaiwana
 */

namespace SMSJuiceAPI\Objects;
use Exception;
class ContactGroup {
    
    private $key;
    private $secret;
    private $groupName;
    private $contacts = [];
    
    /**
     * Creates a new ContactGroup object
     * @param type $key API Key found in the settings section of your account
     * @param type $secret API Secret found in the settings section of your account
     * @param type $groupName Name of group to be created or edited
     * @throws Exception Thrown when either key, secret or groupName are empty or not set
     */
    public function __construct($key, $secret, $groupName) {
        $args = func_get_args();
        foreach($args as $arg){
            if(!isset($arg) || empty($arg)){
                throw new Exception;
            }
        }
        
        $this->key = $key;
        $this->secret = $secret;
        $this->groupName = $groupName;
        
    }
    
    
    /**
     * Adds a list (array) of contacts to the group's contact list.
     * @param type $contacts Contact list to add.
     */
    public function addContactList($contacts){
        $this->contacts = \array_merge($this->contacts,$contacts);
    }
    
    
    /**
     * Adds a single contact to the group
     * @param type $contact Contact (name and) number to add to the contactGroup
     */
    public function addContact($contact){
        $this->contacts[] = $contact;
    }
    
    /**
     * Returns the JSON representation as it will be sent to the SMSJuice HTTP API
     * @return String JSON string to be sent to the API as part of a request
     */
    public function toJSON(){
        $vars_array = get_object_vars($this);
        return json_encode($vars_array);
    }
}

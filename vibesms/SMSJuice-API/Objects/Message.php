<?php

/**
 * Wrapper for sending an SMS message to a recipient or group of recipients
 *
 * @author SMSJuice Development Team
 */

namespace SMSJuiceAPI\Objects;
use Exception;
use DateTime;

class Message {

    const RECIPIENT = 0;
    const SENDER = 1;
    const LONG_MESSAGE_SET = true;
    const NORMAL_MESSAGE_SET = true;
    
    private $key;
    private $secret;       
    private $from;
    private $message;
    private $to = [];
    private $schedule;
    private $expiry;
    
    private $encoding = 0;
    private $recipientType = "numbers";




    /**
     * Creates a Message Object which will store all the details needed to send a message to recipients through the SMSJuice API
     * @param type $key API Key found in the settings section of your account
     * @param type $secret API Secret found in the settings section of your account
     */
    public function __construct($key, $secret) {
        $this->key = $key;
        $this->secret = $secret;
    }

    
    
    /**
     * Set the senderID, this is equivalent to the "From" field
     * @param type $sender This is the name/number the recipient will receive the message from.
     * @throws Exception Thrown when sender is null, empty or invalid
     */
    public function setSender($sender){
        if(isset($sender) && !empty($sender)){
            $checked_sender = $this->validateMSISDN($sender, self::SENDER);
            if($checked_sender){
                $this->from = $checked_sender;
            }else{
                throw new Exception("Invalid sender '{$sender}'");
            }
        }else{
            throw new Exception("Empty or null sender");
        }
    }
    
    
/**
 * Sets the recipient list, and filters out all invalid recipient numbers
 * @param type $recipient_list Array of recipients or groups that will receive the message.
 */
    public function setRecipientList($recipient_list){
        if(!isset($recipient_list)){
            throwException("Null recipient_list submitted");
        }         
        if($this->recipientType==="numbers"){
            foreach($recipient_list as $recipient){
                $checked_recipient = $this->validateMSISDN($recipient, self::RECIPIENT);
                if($checked_recipient){
                    $this->to[] = $checked_recipient;
                }
            }
        }else{
            $this->to = array_merge($this->to,$recipient_list);
        }
    }
    
    
    /**
     * Adds a number to the recipient list
     * @param type $recipient
     */
    public function addToRecipientList($recipient){
        if($this->recipientType==="numbers"){
            $checked_recipient = $this->validateMSISDN($recipient, self::RECIPIENT);
            if($checked_recipient){
                $this->to[] = $checked_recipient;
            }
        }else{
            $this->to[] = $recipient;
        }
    }
    
    /**
     * Set the type of recipient, either "numbers" for contacts put in the recipients list OR "groups" for names of groups on your account.
     * @param type $type Sets the type of recipients you will be sending to. Whether it will be a list of groups on your account or a number
     * @throws Exception
     */
    public function setRecipientType($type){
        if(strtolower($type)==="numbers" || strtolower($type)==="groups"){
            $this->recipientType = $type;
        }else{
            throw new Exception("Invalid recipient type. Must be 'groups' OR 'nummbers'.");
        }
    }
    
    /**
     * The message to be sent to the recipient(s)
     * @param String $message The message to be sent to the recipients
     */
    public function setMessage($message){
        if(!isset($message) || empty($message)){
            throw new Exception("Empty or null message");
        }
        
        $this->message = $message;
                
    }
    
    
    /**
     * The date and time at which the message can be sent. It is not mandatory to set this. If set, it has to be in 'Y-m-d H:i:s' format. Example -> '2015-03-27 15:00'
     * @param String $schedule The unchecked date value
     */
    public function setSendDate($schedule){
        if($this->validateDate($schedule)){
            $this->schedule = $schedule;
        }else{
            throw new Exception("Invalid schedule date");
        }
    }
    
    /**
     * setExpiryDate - The date and time at which the message would expire if not delivered yet. It is not mandatory to set this. If set, it has to be in 'Y-m-d H:i' format. Example -> '2015-03-27 15:00'. Only works if send date has been set, and the default expiry period is 2 days fromt he date of sending
     * @param String $expiry The unchecked date value
     */    
    public function setExpiryDate($expiry){
            if($this->validateDate($expiry)){
                $this->expiry = $expiry;
            }else{
                throw new Exception("Invalid expiry date");
            }
    }
    
    /**
     * Message encoding, which identifies what characterset is used to write and send the message
     * @param type $encoding encoding value of the message. Recommended values : 0,3 and 8
     */
    public function setEncoding($encoding){
        $this->encoding = $encoding;
    }  
    
    
    
    /**
     * Returns a JSON string representation of the Message object. Null values are ignored
     * @return String JSON string representing the serialized object
     * @throws Exception Thrown when mandatory fields are left null, empty or invalid
     */
    public function toJSON(){
        $mandatory_vars = ["key","secret","from","message","to"];
        foreach($mandatory_vars as $var){
            if(is_array($this->{$var})){
                if(count($this->{$var})===0){
                    throw new Exception("Empty recipient list");
                }
            }else{
                if(!isset($this->{$var}) || empty($this->{$var})){
                    throw new Exception("Empty or null variable '{$var}' ");
                }
            }
        }
        
        if(isset($this->expiry) && !isset($this->schedule)){
            $minutes_difference = $this->getDateDifferenceInMinutes(new \DateTime(), new \DateTime($this->expiry));
        }elseif(isset($this->expiry) && isset($this->schedule)){
            $minutes_difference = 
                    $this->getDateDifferenceInMinutes(new \DateTime($this->schedule), new \DateTime($this->expiry));
        }else{
            $minutes_difference=30;
        }
        
        if($minutes_difference<29){
            throw new Exception("Expiry Date has to be at least 30 minutes more than"
                    . " the sending (or current) time");
        }
        
        
        if($this->encoding===0){
            $this->message = $this->enforceGSM7ExtendedCharacters($this->message);
        }
        
        $vars_array = array_filter(get_object_vars($this));
        
        return json_encode($vars_array);
    }

    
    public function getDateDifferenceInMinutes(\DateTime $earlierDate,\DateTime  $laterDate){
        $minutes_difference = abs($laterDate->getTimestamp() - $earlierDate->getTimestamp())/60;
        return (int) $minutes_difference;
    }
    
    /**
     * validateDate Validates the date accoding to the following format: 'Y-m-d H:i:s'. Returns true if the submitted format is correct
     * @param String $date
     * @return boolean
     */
    private function validateDate($date){
        $result = true;
        if(DateTime::createFromFormat("Y-m-d H:i", $date)===false){
            $result = false;    
        }
        return $result;
    }
    
    /**
     * validateMSISDN - Validates the sender and/or the recipient addresses to make sure they conform with the SMPP3.4 starndard. Returns formatted address or FALSE if submitted address is not valid
     * @param String $number
     * @param int $number_type
     * @return boolean
     */
    private function validateMSISDN($number,$number_type){
        $number = strval($number);
        $number = trim($number);
        if(strlen($number)>=16){
        //wrong number, return false
        return FALSE;
        }
        if(substr($number,0,1)=='+'){
            if(!ctype_digit(substr($number,1))){
                    return FALSE;
            }else{
                    $number=str_replace("+","",$number);
            }
        }

        if(!ctype_digit($number)){
            if($number_type==self::SENDER){//if number is a sender then it's allowed to be alpha-numeric
            if(ctype_alnum($number)){
                if(strlen($number)>11){
                    //longer than 11 characters required for alphanumeric
                    return FALSE;
                }else{
                    //perfect alphanum 11 characters or less
                    return $number;
                }
            }else{
                //not digit, not alpha, not mixed digit-alpha
                return FALSE;
            }
            }else{
                return FALSE;//it's a recipient number, has to be all digits
            }
        }
        
        if(substr($number,0,1)=='0'){
            if(substr($number,0,2)=='00'){
                    $number=preg_replace('/00/','',$number,1);
                    return $number;
            }else{
                    return FALSE;
            }
        }else{
            //number doesn't start with 0 or 00 or +(0(0))... the perfect number
            return $number;
        }
    }
    
    /**
     * enforceGSM7ExtendedCharacters - makes sure that some characters are shown properly on the mobile device.
     * @param String $message
     * @return String
     */
    private function enforceGSM7ExtendedCharacters($message){
        
        if(substr(phpversion(), 0, 1)=="7"){//if it's php7.x.x
            $message = str_replace("€", "\u{1b}\u{65}", $message);
            $message = str_replace("£", chr("01"), $message);
            $message = str_replace("@", chr("00"), $message);
            $message = str_replace("$", chr("02"), $message);
        }else{
            $message = str_replace("€", chr("0x1b").chr("0x65"), $message);
            $message = str_replace("£", chr("0x01"), $message);
            $message = str_replace("@", chr("0x00"), $message);
            $message = str_replace("$", chr("0x02"), $message);
        }
        //$message = str_replace("€",chr("0xa4"), $message);
        return $message;
    }    
}

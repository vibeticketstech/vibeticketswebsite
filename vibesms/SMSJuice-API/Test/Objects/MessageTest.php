<?php
require_once(__DIR__."/../../Objects/Message.php");

/**
 * Test the Message object
 *
 * @author Wilson Mazaiwana
 */

use SMSJuiceAPI\Objects\Message;

class MessageTest extends PHPUnit_Framework_TestCase {

    /**
     * @var \RemoteWebDriver
     */
    protected $webDriver;

    public function setUp() {
    }

    public function tearDown() {
    }
    
    public function testCreateMessageWithEmptyManadoryParametersThrowsException(){
        //empty credentials
        $m = new Message("kei", "seikret");
        $m->setSender("test");
        $m->setRecipientList([]);
        $m->setMessage("Hello world!");
        try{
            echo $m->toJSON();
            $this->fail("Unexpected. Supposed to throw an exception");
        } catch (Exception $e){
            
        }
    }

    public function testCreateMessageWithNullManadoryParametersThrowsException(){
        //empty credentials
        $m = new Message(null, null);
        $m->setSender("test");
        $m->setRecipientList(["447905081486"]);
        try{
            echo $m->toJSON();
            $this->fail("Unexpected. Supposed to throw an exception");
        } catch (Exception $e){
            
        }
    }
    
    public function testSetInvalidRecipientTypeThrowsException(){
        $m = new Message("kei", "seikret");
        try{
            $m->setRecipientType("blabla");
        } catch (Exception $e){
            //expected
        }        
    }
    
    public function testSetNullRecipientTypeThrowsException(){
        $m = new Message("kei", "seikret");
        try{
            $m->setRecipientType(null);
        } catch (Exception $e){
            //expected
        }
        
    }
    
    public function testSetEmptyMessageThrowsException(){
        $m = new Message("kei", "seikret");
        $m->setSender("test");
        $m->setRecipientList(["447905081486"]);
        try{
            $m->setMessage("");//empty message
            $this->fail("Unexpected. Supposed to throw an exception");
        } catch (Exception $e){
            //expected
        }        
    }
    
    public function testSetNullMessageThrowsException(){
        $m = new Message("kei", "seikret");
        $m->setSender("test");
        $m->setRecipientList(["447905087882"]);
        try{
            $m->setMessage(null);//null message
            $this->fail("Unexpected. Supposed to throw an exception");
        } catch (Exception $e){
            //expected
        }         
    }
    
    public function testSetMessageIsOk(){
        $m = new Message("kei", "seikret");
        $m->setSender("test");
        $m->setRecipientList(["447905087882"]);
        try{
            $m->setMessage("hello World!");
        } catch (Exception $e){
            $this->fail("Unexpected... No exception is to be thrown");
            //expected
        }         
    }
    
    public function testSetNullSendDateThrowsException(){
        $m = new Message("kei", "seikret");
        try{
            $m->setSendDate(null);
            $this->fail("Unexpected. Supposed to throw an exception");
        } catch (Exception $e){
            //expected
        }        
    }
    
    public function testSetInvalidSendDate1ThrowsException(){
        $m = new Message("kei", "seikret");
        try{
            $m->setSendDate("2016-e4-15 00:00");
            $this->fail("Unexpected. Supposed to throw an exception");
        } catch (Exception $e){
            //expected
        }        
    }

    public function testSetInvalidSendDate2ThrowsException(){
        $m = new Message("kei", "seikret");
        try{
            $m->setSendDate("2016-e4-15 00:00:00");
            $this->fail("Unexpected. Supposed to throw an exception");
        } catch (Exception $e){
            //expected
        }        
    }     
    
    public function testSetInvalidExpiryDateThrowsException(){
        $m = new Message("kei", "seikret");
        $m->setSender("test");
        $m->setRecipientList(["447905087882"]);
        try{
            $m->setMessage("hello World");
            $m->setSendDate("2016-04-15 00:00");
            $m->setExpiryDate("2016-04-15 00:20");
            echo $m->toJSON();
            $this->fail("Unexpected. Supposed to throw an exception");
        } catch (Exception $e){
            //expected
        }        
    }
    
    public function testSetValidExpiryDateIsOk(){
        $m = new Message("kei", "seikret");
        $m->setSender("test");
        $m->setRecipientList(["447905087882"]);
        try{
            $m->setMessage("hello World");
            $m->setSendDate("2016-04-15 00:00");
            $m->setExpiryDate("2016-04-15 00:35");
            $m->toJSON();
        } catch (Exception $e){
            $this->fail("Unexpected. ");
            
        }        
    }    
    
    public function testToJSONWithInvalidOrMissingMessagePropertiesThrowsException(){
        //missing senderID / TO
        $m = new Message("kei", "seikret");
        $m->setRecipientList(["447905087882"]);
        try{
            $m->setMessage("hello World");
            $m->setSendDate("2016-04-15 00:00");
            $m->setExpiryDate("2016-04-15 00:30");
            $m->toJSON();
            $this->fail("Unexpected. Supposed to throw an exception");
        } catch (Exception $e){
            //expected
        }    
    }
    
    public function testToJSONWithValidMessagePropertiesIsOk(){
        $m = new Message("kei", "seikret");
        $m->setSender("test");
        $m->setRecipientList(["447905087882"]);
        try{
            $m->setMessage("hello World");
            $m->setSendDate("2016-04-15 00:00");
            $m->setExpiryDate("2016-04-15 00:30");
            $m->toJSON();
        } catch (Exception $e){
            $this->fail("Unexpected.");
        }
    }
    
}

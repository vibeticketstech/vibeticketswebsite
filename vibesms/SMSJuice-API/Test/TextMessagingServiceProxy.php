<?php
require_once("../TextMessagingService.php");
/**
 * Class used for tests only, to get round the inability of phpunit to test static methods. Shocking in this day and age
 *
 * @author Wilson Mazaiwana
 */

use SMSJuiceAPI\TextMessagingService;

class TextMessagingServiceProxy {
    
    public function getProxy($key,$secret){
        return TextMessagingService::Get(TextMessagingService::GET_BALANCE, [$key,$secret]);
    }
    
    public function postProxy($json){
        return TextMessagingService::Post($json, TextMessagingService::GET_BALANCE);
    }
        
    public function deleteProxy($key,$secret,$groupToDelete){
        return TextMessagingService::Get(TextMessagingService::GROUPS_URI, [$key,$secret,$groupToDelete]);
    }
}

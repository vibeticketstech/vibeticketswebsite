<?php
require(__DIR__.'/../../vendor/autoload.php');
include("AbstractIntegrationTest.php");
include("../Objects/ContactGroup.php");
include("../TextMessagingService.php");

use SMSJuiceAPI\TextMessagingService;
use SMSJuiceAPI\Objects\ContactGroup;

class ContactGroupIntegrationtest extends AbstractIntegrationTest{
    
    public function testAddNewGroup(){
        $group = new ContactGroup($this->properties['key'],
                $this->properties['secret'], $this->properties['group']);
        $group->addContact($this->properties['contact']);
        $result = TextMessagingService::addGroup($group);
        $data = json_decode($result,true);
        //check if it was successful
        $this->assertEquals("success",$data['status']);
        //check if there were any rejected numbers
        $this->assertTrue(count($data['invalidContacts'])==0);
    }
    
    
    public function testAddNewContactToGroup(){
        $group = new ContactGroup($this->properties['key'],
                $this->properties['secret'], $this->properties['group']);
        $group->addContact($this->properties['anothercontact']);
        $result = TextMessagingService::addContactToGroup($group);
        $data = json_decode($result,true);
        //check if it was successful
        $this->assertEquals("success",$data['status']);
        //check if there were any rejected numbers
        $this->assertTrue(count($data['invalidContacts'])==0);
    }
    
    public function testGetAllGroups(){
        $result = TextMessagingService::getAllGroups($this->properties['key'], $this->properties['secret']);
        $data = json_decode($result,true);
        //when the key "groupList" exists the call was successful. 
        //Whether the group list is empty or not depends on the groups on your account
        assertTrue(array_key_exists('groupList', $data));
    }

    
    public function testGetAllGroupContacts(){
        $result = TextMessagingService::getAllGroupContacts($this->properties['key'],
                $this->properties['secret'], $this->properties['group']);
        $data = json_decode($result,true);
        //check if the call was successful by checking the group name in the result
        assertEquals($this->properties['group'],$data['groupName']);
        assertTrue(array_key_exists('contacts', $data));
    }
    
    public function testDeleteGroup(){
        $result = TextMessagingService::deleteGroup($this->properties['key'],
                $this->properties['secret'], $this->properties['group']);
        $data = json_decode($result, true);
        assertEquals("successful", $data['operation']);
    }
    
    
    public function testDeleteContactFromGroup(){
        $result = TextMessagingService::deleteContactFromGroup($this->properties['key'],
                $this->properties['secret'], $this->properties['group'], $this->properties['contact']);
        $data = json_decode($result, true);
        assertEquals("successful", $data['operation']);
    }
}
module.exports = [
    {
        entry: {
            "editor-button": "./cms/app/views/admin/editor-button",
            "shortcode-edit": "./cms/app/views/admin/shortcode-edit",
            "shortcodes": "./cms/app/views/admin/shortcodes",
            "settings": "./cms/app/views/admin/settings"
        },
        output: {
            filename: "./cms/app/bundle/[name].js"
        },
        module: {
            loaders: [
                { test: /\.vue$/, loader: "vue" }
            ]
        }
    }
];

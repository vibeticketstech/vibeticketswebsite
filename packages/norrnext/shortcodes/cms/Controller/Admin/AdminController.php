<?php
/**
 * @package    Pkb Shortcodes
 * @author     Zhukov Sergey <zom688@gmail.com>
 * @copyright  Copyright © 2016 - 2017 NorrNext. All rights reserved.
 * @license    GNU General Public License version 3 or later; see license.txt
 */
namespace Norrnext\Shortcodes\Controller\Admin;

use Norrnext\Shortcodes\Model\Admin\Shortcode;
use Pagekit\Application as App;

/**
 * @Access(admin=true)
 */
class AdminController
{
    /**
     * @Route("/", methods="GET")
     */
    public function indexAction($filter = [], $page = 0)
    {
        return [
            '$view' => [
                'title' => __('Shortcodes'),
                'name'  => 'nxtshc/views:/admin/shortcodes.php'
            ],
            '$data' => [
                  'statuses' => Shortcode::getStatuses(),
                  'canEditAll' => true, //App::user()->hasAccess('shortcode: manage all shortcodes'),
                  'config'   => [
                      'filter' => (object) $filter,
                      'page'   => $page
                  ]
              ]
        ];
    }

    /**
     * @Route("/shortcodes", methods="GET")
     * @Request({"filter": "array", "page":"int"})
     */
    public function shortcodesAction($filter = [], $page = 0)
    {
        $model = new Shortcode();
        $data = $model->getItems($filter, $page);
        return $data;
    }

    /**
     * @Route("/", methods="POST")
     * @Route("/save", methods="POST")
     * @Request({"shortcode": "array", "id": "int"}, csrf=true, requirements={"id"="\d+"})
     */
    public function saveAction($data, $id = 0)
    {
        if (!$id || !$shortcode = Shortcode::find($id)) {

            if ($id) {
                App::abort(404, __('Shortcode not found.'));
            }

            $shortcode = Shortcode::create();
        }

        // user without universal access is not allowed to assign posts to other users
        //if(!App::user()->hasAccess('shortcode: manage all shortcodes')) {
        //    $data['user_id'] = App::user()->id;
        //}

        // user without universal access can only edit their own posts
        //if(!App::user()->hasAccess('shortcode: manage all shortcodes') && !App::user()->hasAccess('shortcode: manage own shortcodes') && $post->user_id !== App::user()->id) {
        //    App::abort(400, __('Access denied.'));
        //}

        $shortcode->save($data);

        return ['message' => 'success', 'shortcode' => $shortcode];
    }

    /**
     * @Route("/{id}", methods="DELETE", requirements={"id"="\d+"})
     * @Request({"id": "int"}, csrf=true)
     */
    public function deleteAction($id)
    {
        if ($shortcode = Shortcode::find($id)) {

            //if(!App::user()->hasAccess('shortcode: manage all shortcodes') && !App::user()->hasAccess('shortcode: manage own shortcodes') && $post->user_id !== App::user()->id) {
            //    App::abort(400, __('Access denied.'));
            //}

            $shortcode->delete();
        }

        return ['message' => 'success'];
    }

    /**
     * @Route("/copy", methods="POST")
     * @Request({"ids": "int[]"}, csrf=true)
     */
    public function copyAction($ids = [])
    {
        foreach ($ids as $id) {
            if ($shortcode = Shortcode::find($id)) {

                //if(!App::user()->hasAccess('shortcode: manage all shortcodes') && !App::user()->hasAccess('shortcode: manage own shortcodes') && $post->user_id !== App::user()->id) {
                //    App::abort(400, __('Access denied.'));
                //}

                $shortcode = clone $shortcode;
                $shortcode->id = null;
                $shortcode->status = Shortcode::STATUS_PUBLISHED;
                $shortcode->name = $shortcode->name.' - '.__('Copy');
                $shortcode->create_date = new \DateTime();
                $shortcode->update_date = new \DateTime();
                $shortcode->save();
            }
        }

        return ['message' => 'success'];
    }

    /**
     * @Route("/bulk", methods="POST")
     * @Request({"shortcodes": "array"}, csrf=true)
     */
    public function bulkSaveAction($shortcodes = [])
    {
        foreach ($shortcodes as $data) {
            $this->saveAction($data, isset($data['id']) ? $data['id'] : 0);
        }

        return ['message' => 'success'];
    }

    /**
     * @Route("/bulk", methods="DELETE")
     * @Request({"ids": "array"}, csrf=true)
     */
    public function bulkDeleteAction($ids = [])
    {
        foreach (array_filter($ids) as $id) {
            $this->deleteAction($id);
        }

        return ['message' => 'success'];
    }

    /**
     * @Route("/edit", methods="GET")
     * @Request({"id": "int"})
     */
    public function editAction($id = 0)
    {
        try {

            if (!$shortcode = Shortcode::where(compact('id'))->first()) {

                if ($id) {
                    App::abort(404, __('Invalid shortcode id.'));
                }

                $module = App::module('norrnext/shortcodes');

                $shortcode = Shortcode::create([
                    'status' => Shortcode::STATUS_PUBLISHED,
                    'create_date' => new \DateTime(),
                    'update_date' => new \DateTime()
                ]);
            }

            $user = App::user();

            if(!$user->hasAccess('shortcode: manage all shortcodes')) {
                App::abort(403, __('Insufficient User Rights.'));
            }

           /* $roles = App::db()->createQueryBuilder()
                ->from('@system_role')
                ->where(['id' => Role::ROLE_ADMINISTRATOR])
                ->whereInSet('permissions', ['shortcode: manage all shortcodes', 'shortcode: manage own shortcode'], false, 'OR')
                ->execute('id')
                ->fetchAll(\PDO::FETCH_COLUMN);

            $authors = App::db()->createQueryBuilder()
                ->from('@system_user')
                ->whereInSet('roles', $roles)
                ->execute('id, username')
                ->fetchAll();*/

            return [
                '$view' => [
                    'title' => $id ? __('Edit Shortcode') : __('Add Shortcode'),
                    'name'  => 'nxtshc/views:/admin/shortcode-edit.php'
                ],
                '$data' => [
                    'shortcode'     => $shortcode,
                    'statuses' => Shortcode::getStatuses(),
                    /*'roles'    => array_values(Role::findAll()),*/
                    'canEditAll' => true, //App::user()->hasAccess('shortcode: manage all shortcodes'),
                ],
                'shortcode' => $shortcode
            ];

        } catch (\Exception $e) {

            App::message()->error($e->getMessage());

            return App::redirect('@shortcodes/admin');
        }
    }

    /**
     * @Access("system: manage settings")
     */
    public function settingsAction()
    {
        return [
            '$view' => [
                'title' => __('settings'),
                'name'  => 'nxtshc/views:admin/settings.php'
            ],
            '$data' => [
            ]
        ];
    }
}
 
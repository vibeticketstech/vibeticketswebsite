window.Shortcode = {

    el: '#shortcode',

    data: function () {
        return {
            data: window.$data,
            shortcode: window.$data.shortcode,
            sections: []
        }
    },

    created: function () {

        var sections = [];

        _.forIn(this.$options.components, function (component, name) {

            var options = component.options || {};

            if (options.section) {
                sections.push(_.extend({name: name, priority: 0}, options.section));
            }

        });

        this.$set('sections', _.sortBy(sections, 'priority'));

        this.resource = this.$resource('admin/shortcodes{/action}');
    },

    ready: function () {
        this.tab = UIkit.tab(this.$els.tab, {connect: this.$els.content});
        this.shortcode.icon = this.shortcode.icon || '';
    },

    methods: {

        save: function () {
            var data = {shortcode: this.shortcode, id: this.shortcode.id};

            this.$broadcast('save', data);

            this.resource.save({action: 'save'}, data).then(function (res) {

                var data = res.data;

                if (!this.shortcode.id) {
                    window.history.replaceState({}, '', this.$url.route('admin/shortcodes/edit', {id: data.shortcode.id}))
                }

                this.$set('shortcode', data.shortcode);

                this.$notify('Shortcode saved.');

            }, function (res) {
                this.$notify(res.data, 'danger');
            });
        }

    },

    components: {
        settings: require('../../components/shortcode-form.vue')
    }
};

Vue.ready(window.Shortcode);
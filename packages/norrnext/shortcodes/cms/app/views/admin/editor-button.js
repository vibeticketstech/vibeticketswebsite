module.exports = {

    data: {
        shortcodes: [],
        btn: 'a[data-htmleditor-button="shortcodes"]'
    },

    created: function() {

        var vm = this;
        //TODO: find another way or event to disable button after toolbar is loaded
        var interval = setInterval(function() {
            var $btn = $(vm.btn);
            if($btn.length) {
                $btn.addClass('disabled');
                clearInterval(interval);
            }
        }, 10);

        this.$http.get('admin/shortcodes/shortcodes', { filter: {'status' : 1} }, function (data) {

            clearInterval(interval);
            if(data.shortcodes.length) {
                $(vm.btn).removeClass('disabled');
                vm.shortcodes = data.shortcodes;
            }

        }).error(function (data) {
            console.log(data);
        });
    },

    methods: {
        click: function (e, editor) {

            var cursor = editor.getCursor(), vm = this;

            if($(vm.btn).hasClass('disabled')) {
                return false;
            }

            new this.$options.utils['shortcode-picker']({
                parent: this,
                data: {
                    shortcode: '',
                    shortcodes: vm.shortcodes
                }
            }).$mount()
                .$appendTo('body')
                .$on('select', function (shortcode) {
                    editor.replaceRange(shortcode, cursor);
                });
        }
    },
    utils: {
        'shortcode-picker': Vue.extend(require('../../components/shortcode-picker.vue'))
    }
};

//TODO find another way to extend editor toolbar if exist
Vue.mixin({
    created: function () {
        if (_.has(this, '$parent.editor')) {
            this.$on("ready", function () {
                if (_.has(this, '$parent.editor.htmleditor')) {
                    var editor = this.$parent.editor, myButton = new Vue(module.exports);
                    editor.options.toolbar.push('shortcodes');
                    editor.addButton('shortcodes', {title: 'Shortcodes', label: '<i class="uk-icon-object-group"></i>'});
                    editor.on('action.shortcodes', function (e, editor) {
                          myButton.click(e, editor);
                    });
                }
            }, this);
        }
    }
});
module.exports = {

    name: 'shortcodes',

    el: '#shortcodes',

    data: function() {
        return _.merge({
            shortcodes: false,
            config: {
                filter: this.$session.get('shortcodes.filter', {order: 'create_date desc', limit:25})
            },
            pages: 0,
            count: '',
            selected: [],
            canEditAll: false
        }, window.$data);
    },

    ready: function () {

        this.resource = this.$resource('admin/shortcodes{/action}');
        this.load();
    },

    watch: {

        'config.page': 'load',

        'config.filter': {
            handler: function (filter) {
                this.load(0);
                this.$session.set('shortcodes.filter', filter);
            },
            deep: true
        }

    },

    computed: {

        statusOptions: function () {

            var options = _.map(this.$data.statuses, function (status, id) {
                return { text: status, value: id };
            });

            return [{ label: this.$trans('Filter by'), options: options }];
        }
    },

    methods: {

        active: function (shortcode) {
            return this.selected.indexOf(shortcode.id) != -1;
        },

        save: function (shortcode) {
            this.resource.save({ action: 'save' }, { shortcode: shortcode, id: shortcode.id }).then(function () {
                this.load();
                this.$notify('Shortcode saved.');
            });
        },

        status: function(status) {

            var shortcodes = this.getSelected();

            shortcodes.forEach(function(shortcode) {
                shortcode.status = status;
            });

            this.resource.save({ action: 'bulk' }, { shortcodes: shortcodes }).then(function () {
                this.load();
                this.$notify('Shortcodes saved.');
            });
        },

        remove: function() {

            this.resource.delete({ action: 'bulk' }, { ids: this.selected }).then(function () {
                this.load();
                this.$notify('Shortcodes deleted.');
            });
        },

        toggleStatus: function (shortcode) {
            shortcode.status = shortcode.status === 0 ? 1 : 0;
            this.save(shortcode);
        },

        copy: function() {

            if (!this.selected.length) {
                return;
            }

            this.resource.save({action: 'copy'}, { ids: this.selected }).then(function () {
                this.load();
                this.$notify('Shortcodes copied.');
            });
        },

        load: function (page) {

            page = page !== undefined ? page : this.config.page;

            this.resource.query({action: 'shortcodes'}, { filter: this.config.filter, page: page }).then(function (res) {

                var data = res.data;

                this.$set('shortcodes', data.shortcodes);
                this.$set('pages', data.pages);
                this.$set('count', data.count);
                this.$set('config.page', page);
                this.$set('selected', []);
            });
        },

        getSelected: function() {
            return this.shortcodes.filter(function(shortcode) { return this.selected.indexOf(shortcode.id) !== -1; }, this);
        },

        getStatusText: function(shortcode) {
            return this.statuses[shortcode.status];
        }

    }

};

Vue.ready(module.exports);

<?php
/**
 * @package    Pkb Shortcodes
 * @author     Zhukov Sergey <zom688@gmail.com>
 * @copyright  Copyright © 2016 - 2017 NorrNext. All rights reserved.
 * @license    GNU General Public License version 3 or later; see license.txt
 */
namespace Norrnext\Shortcodes\Model\Admin;

use Pagekit\Database\ORM\ModelTrait;
use Pagekit\Application as App;

/**
 * @Entity(tableClass="@norrnext_shortcodes")
 */
class Shortcode implements \JsonSerializable
{
    use ModelTrait;

    /** @Column(type="integer") @Id **/
    public $id;
    /** @Column(type="integer") **/
    public $category_id;
    /** @Column(type="string") **/
    public $name;
    /** @Column(type="string") **/
    public $icon;
    /** @Column(type="string") **/
    public $macro;
    /** @Column(type="string") **/
    public $html;
    /** @Column(type="json_array") **/
    public $permissions;
    /** @Column(type="datetime") **/
    public $create_date;
    /** @Column(type="datetime") **/
    public $update_date;
    /** @Column(type="smallint") */
    public $status;

    /* Post published. */
    const STATUS_PUBLISHED = 1;
    /* Post unpublished. */
    const STATUS_UNPUBLISHED = 0;

    protected static $properties = [
        'published' => 'isPublished'
    ];

    public function isPublished()
    {
        return $this->status === self::STATUS_PUBLISHED && $this->update_date < new \DateTime;
    }

    public function getItems($filter = [], $page = 0)
    {
        $query  = static::query();
        $filter = array_merge(array_fill_keys(['status', 'search', 'order', 'limit'], ''), $filter);

        extract($filter, EXTR_SKIP);

        if (is_numeric($status)) {
            $query->where(['status' => (int) $status]);
        }

        if ($search) {
            $query->where(function ($query) use ($search) {
                $query->orWhere(['name LIKE :search', 'html LIKE :search', 'macro LIKE :search'], ['search' => "%{$search}%"]);
            });
        }

        if (!preg_match('/^(create_date|name|id)\s(asc|desc)$/i', $order, $order)) {
            $order = [1 => 'create_date', 2 => 'desc'];
        }

        $limit = (int) $limit ?: App::module('norrnext/shortcodes')->config('shortcodes.shortcodes_per_page');
        $count = $query->count();

        if($limit) {
            $pages = ceil($count / $limit);
            $page = max(0, min($pages - 1, $page));
            $shortcodes = array_values($query->offset($page * $limit)->limit($limit)->orderBy($order[1], $order[2])->get());
            return compact('shortcodes', 'pages', 'count');
        } else {
            $shortcodes = array_values($query->orderBy($order[1], $order[2])->get());
            return compact('shortcodes', 'pages', 'count');
        }
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_UNPUBLISHED => __('Unpublished'),
            self::STATUS_PUBLISHED => __('Published')
        ];
    }
}
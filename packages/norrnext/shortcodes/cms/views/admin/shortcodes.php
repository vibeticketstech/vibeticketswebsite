<?php $view->script('shortcodes', 'nxtshc/cms:app/bundle/shortcodes.js', 'vue') ?>

<div id="shortcodes" class="uk-form" v-cloak>

<div class="uk-margin uk-flex uk-flex-space-between uk-flex-wrap" data-uk-margin>
    <div class="uk-flex uk-flex-middle uk-flex-wrap" data-uk-margin>

        <h2 class="uk-margin-remove" v-if="!selected.length">{{ '{0} %count% Shortcodes|{1} %count% Shortcode|]1,Inf[ %count% Shortcodes' | transChoice count {count:count} }}</h2>

        <template v-else>
            <h2 class="uk-margin-remove">{{ '{1} %count% Shortcode selected|]1,Inf[ %count% Shortcodes selected' | transChoice selected.length {count:selected.length} }}</h2>

            <div class="uk-margin-left" >
                <ul class="uk-subnav pk-subnav-icon">
                    <li><a class="pk-icon-check pk-icon-hover" title="Publish" data-uk-tooltip="{delay: 500}" @click="status(2)"></a></li>
                    <li><a class="pk-icon-block pk-icon-hover" title="Unpublish" data-uk-tooltip="{delay: 500}" @click="status(3)"></a></li>
                    <li><a class="pk-icon-copy pk-icon-hover" title="Copy" data-uk-tooltip="{delay: 500}" @click="copy"></a></li>
                    <li><a class="pk-icon-delete pk-icon-hover" title="Delete" data-uk-tooltip="{delay: 500}" @click="remove" v-confirm="'Delete Shortcodes?'"></a></li>
                </ul>
            </div>
        </template>

        <div class="pk-search">
            <div class="uk-search">
                <input class="uk-search-field" type="text" v-model="config.filter.search" debounce="300">
            </div>
        </div>

    </div>
    <div data-uk-margin>

        <a class="uk-button uk-button-primary" :href="$url.route('admin/shortcodes/edit')">{{ 'Add Shortcode' | trans }}</a>

    </div>
</div>

<div class="uk-overflow-container">
    <table class="uk-table uk-table-hover uk-table-middle">
        <thead>
        <tr>
            <th class="pk-table-width-minimum"><input type="checkbox" v-check-all:selected.literal="input[name=id]" number></th>
            <th class="pk-table-width-minimum uk-text-center">{{ 'Icon' | trans }}</th>
            <th class="pk-table-min-width-200" v-order:name="config.filter.order">{{ 'Name' | trans }}</th>
            <th class="pk-table-min-width-300" v-order:name="config.filter.order">{{ 'Macros' | trans }}</th>
            <th class="pk-table-width-100 uk-text-center">
                <input-filter :title="$trans('Status')" :value.sync="config.filter.status" :options="statusOptions"></input-filter>
            </th>
            <th class="pk-table-width-100" v-order:create_date="config.filter.order">{{ 'Date' | trans }}</th>
        </tr>
        </thead>
        <tbody>
        <tr class="check-item" v-for="shortcode in shortcodes" :class="{'uk-active': active(shortcode)}">
            <td><input type="checkbox" name="id" :value="shortcode.id"></td>
            <td><i class="uk-icon-medium {{ shortcode.icon }}"></i></td>
            <td>
                <a :href="$url.route('admin/shortcodes/edit', { id: shortcode.id })">{{ shortcode.name }}</a>
            </td>
            <td>
                {{ shortcode.macro }}
            </td>
            <td class="uk-text-center">
                <a :title="getStatusText(shortcode)" :class="{
                                'pk-icon-circle-success': shortcode.status == 1 && shortcode.published,
                                'pk-icon-circle-danger': shortcode.status == 0,
                                'pk-icon-schedule': shortcode.status == 1 && !shortcode.published
                            }" @click="toggleStatus(shortcode)"></a>
            </td>
            <td>
                {{ shortcode.create_date | date }}
            </td>
        </tr>
        </tbody>
    </table>
</div>

<h3 class="uk-h1 uk-text-muted uk-text-center" v-show="shortcodes && !shortcodes.length">{{ 'No shortcodes found.' | trans }}</h3>

<v-pagination :page.sync="config.page" :pages="pages" v-show="pages > 1 || page > 0"></v-pagination>

</div>
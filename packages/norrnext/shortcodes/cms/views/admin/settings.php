<?php
/**
 * @package    Pkb Shortcodes
 * @author     Zhukov Sergey <zom688@gmail.com>
 * @copyright  Copyright © 2016 - 2017 NorrNext. All rights reserved.
 * @license    GNU General Public License version 3 or later; see license.txt
 */
$view->style('admin', 'nxtshc/cms:assets/css/admin.css', 'uikit');
$view->script('settings', 'nxtshc/cms:app/bundle/settings.js', ['vue', 'jquery']);
$view->script('platform.js', '//apis.google.com/js/platform.js');
?>
<div id="settings" class="uk-form uk-form-horizontal">
 <h3>{{ 'Description' | trans }}</h3>
 <p>
  {{ 'PKB Shortcodes allows you to easily add complex HTML with shortcodes (macros).
  The widget changes the preset syntax, such as [example] to the relevant HTML code during the rendering
  of the Pagekit content on the front-end side.' | trans }}
 </p>
 <h3>{{ 'Example of use:' | trans }}</h3>
 <p>{{ 'In the Shortcode Macros field, enter:' | trans }}<br>
  <b>[span class="{class}"]{text}[/span]</b>
 </p>
 <p>
  {{ 'In the HTML Macros field, enter:' | trans }}<br>
  <b>&lt;span class="{class}"&gt;{text}&lt;/span&gt;</b>
 </p>
 <p>{{ 'For more information, please read the ' | trans }}<a href="https://www.norrnext.com/docs/pagekit-free-widgets/pkb-shortcodes">{{ 'Documentation' | trans }}</a>.</p>

 <div class="uk-width-medium-1-1 uk-container-center uk-margin-top">
  <div class="uk-panel uk-panel-box uk-panel-box-primary">
   <p class="uk-text-center">{{ 'Extensions made with love by NorrNext.' | trans }}</p>
   <ul class="uk-text-center social-share-list">
    <li>
     <iframe
         src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Ffacebook.com%2Fnorrnext&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21"
         scrolling="no"
         frameborder="0"
         style="border:none; overflow:hidden; height:20px; width:120px"
         allowTransparency="true">
     </iframe>
    </li>
    <li class="share-google">
     <div class="g-follow" data-href="https://plus.google.com/108999239898392136664" data-rel="author"></div>
    </li>
    <li>
     <a href="https://twitter.com/norrnext" class="twitter-follow-button" data-show-count="true">Follow @norrnext</a>
    </li>
    <script>window.twttr = (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0],
          t = window.twttr || {};
      if (d.getElementById(id)) return t;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://platform.twitter.com/widgets.js";
      fjs.parentNode.insertBefore(js, fjs);

      t._e = [];
      t.ready = function(f) {
       t._e.push(f);
      };

      return t;
     }(document, "script", "twitter-wjs"));</script>
   </ul>
   <p class="uk-text-center">
    <a href="https://www.norrnext.com/pagekit-extensions/pkb-shortcodes" target="_blank">{{ 'Home page' | trans }}</a> |
    <a href="https://www.norrnext.com/downloads/free-pagekit/pkb-shortcodes" target="_blank">{{ 'Download' | trans }}</a> |
    <a href="https://www.norrnext.com/docs/pagekit-free-widgets/pkb-shortcodes" target="_blank">{{ 'Documentation' | trans }}</a> |
    <a href="https://www.norrnext.com/forums/categories/listings/pkb-shortcodes" target="_blank">{{ 'Support' | trans }}</a>
   </p>
  </div>
  <div class="uk-text-center"><small>Dashboard icon by <a href="https://www.iconfinder.com/justui">Denis Stelmah</a></small></div>
 </div>
</div>


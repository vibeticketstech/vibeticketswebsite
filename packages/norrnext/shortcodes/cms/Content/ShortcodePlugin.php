<?php
/**
 * @package    Pkb Shortcodes
 * @author     Zhukov Sergey <zom688@gmail.com>
 * @copyright  Copyright © 2016 - 2017 NorrNext. All rights reserved.
 * @license    GNU General Public License version 3 or later; see license.txt
 */

namespace Norrnext\Shortcodes\Content;

use Pagekit\Content\Event\ContentEvent;
use Pagekit\Event\EventSubscriberInterface;
use Norrnext\Shortcodes\Model\Admin\Shortcode;

class ShortcodePlugin implements EventSubscriberInterface
{
    /**
     * Content plugins callback.
     *
     * @param ContentEvent $event
     */
    public function onContentPlugins(ContentEvent $event)
    {
        $content = $event->getContent();

        if($content) {
            $model = new Shortcode();
            $data = $model->getItems(['status' => 1]);

            if(!empty($data['shortcodes'])) {

                foreach ($data['shortcodes'] as $item)
                {
                    $search         = [];
                    $replace        = [];
                    $tokens         = [];

                    $macro = $item->macro;
                    $html =  $item->html;

                    // create a working body
                    $working_body = $content;
                    $script_tag_matches = [];

                    // remove the script tag contents from the working body
                    $find_scipt_tag = '#(<script.*type="text/javascript"[^>]*>(?!<script)(.*)</script>)#iUs';
                    preg_match_all  ($find_scipt_tag, $working_body, $script_tag_matches);
                    foreach($script_tag_matches[2] as $scripttagbody)
                    {
                        if(!empty($scripttagbody)){
                            $working_body = str_replace($scripttagbody,'',$working_body);
                        }
                    }

                    // build the regexp for the tag
                    $opentag = substr($macro,0,strpos($macro,']')+1);
                    $partial_open_tag = substr($opentag,0,(strpos($opentag,' '))?strpos($opentag,' '):strpos($opentag,']'));
                    $tokened_opentag =  preg_replace('/\{([a-zA-Z0-9_]+)\}/', '(?P<$1>.*?)',$opentag);
                    if (strpos($opentag,"/]")){
                        $escaped_key = $this->_addEscapes($tokened_opentag);
                    }
                    else {
                        $tag_contents = substr($macro, strpos($item->macro,']')+1, strrpos($macro,'[') - (strpos($macro,']')+1));
                        $tokened_tag_contens = preg_replace('/\{([a-zA-Z0-9_]+)\}/', '(?P<$1>(?s:(?!'.$partial_open_tag.').)*?)',$tag_contents);
                        $closetag = substr($macro,strrpos($macro,'['),strrpos($macro,']')-strrpos($macro,'[')+1);
                        $escaped_key = $this->_addEscapes($tokened_opentag.$tokened_tag_contens.$closetag);
                    }
                    $final_tag_patern = "%".$escaped_key."%";
                    // run the matching for the tag on the working body
                    preg_match_all($final_tag_patern, $working_body, $results);
                    if (!empty($results[0])) {
                        $search = array_merge($search, $results[0]);
                        foreach ($results as $k => $v) {
                            if (!is_numeric($k)) {
                                $tokens[] = $k;
                            }
                        }
                        for($i=0;$i< count($results[0]);$i++) {
                            $tmpval = $html;
                            foreach ($tokens as $token) {
                                $tmpval = str_replace("{".$token."}",$results[$token][$i],$tmpval);
                            }
                            $replace[] = $tmpval;
                        }
                    }
                    // do actual replacement on the real content
                    $content = str_replace($search, $replace, $content);
                }
                $event->setContent($content);
            }
        }
    }

    function _addEscapes($fullstring) {
        $fullstring            = str_replace("\\","\\\\",$fullstring);
        $fullstring            = str_replace("[","\[",$fullstring);
        $fullstring            = str_replace("]","\\]",$fullstring);
        return $fullstring;
    }


    /**
     * {@inheritdoc}
     */
    public function subscribe()
    {
        return [
            'content.plugins' => ['onContentPlugins', 10]
        ];
    }
}

<?php
use Norrnext\Shortcodes\Content\ShortcodePlugin;

return [

    'name' => 'norrnext/shortcodes',

    'type' => 'extension',

    'autoload' => [
        'Norrnext\\Shortcodes\\' => 'cms'
    ],

    'resources' => [

        'nxtshc:' => '',
        'nxtshc/views:' => 'cms/views',
        'nxtshc/cms:' => 'cms'

    ],

    'routes' => [

        'shortcodes' => [
            'name' => '@shortcodes/admin',
            'controller' => 'Norrnext\\Shortcodes\\Controller\\Admin\\AdminController'
        ]
    ],

    'menu' => [

        'shortcodes' => [
            'label' => 'Shortcodes',
            'icon' => 'nxtshc:icon.svg',
            'active' => '@shortcodes/admin(/edit)?',
            'url' => '@shortcodes/admin'
        ],

        'shortcodes: shortcodes' => [
            'label' => 'Shortcodes',
            'parent' => 'shortcodes',
            'active' => '@shortcodes/admin(/edit)?',
            'icon' => 'nxtshc:icon.svg',
            'url' => '@shortcodes/admin'
        ],

        'shortcodes: settings' => [
            'label' => 'Info',
            'parent' => 'shortcodes',
            'icon' => 'nxtshc:icon.svg',
            'active' => '@shortcodes/admin/settings',
            'url' => '@shortcodes/admin/settings'
        ]
    ],

    'settings' => '@shortcodes/admin/settings',

    'config' => [

        'option-group1' => [

            'param1' => false,
            'param2' => 14,

        ],

    ],

    'events' => [

        'view.scripts' => function ($event, $scripts) {
            $scripts->register('shortcodes-settings', 'nxtshc/cms:/app/bundle/settings.js', '~extensions');
        },

        'boot' => function ($event, $app) {
            $app->subscribe(
                new ShortcodePlugin
            );

            $app->on('view.head', function ($event, $view) use ($app) {
                if($app['isAdmin']) {
                    $view->script('editor-button', 'nxtshc/cms:app/bundle/editor-button.js', ['vue', 'editor', 'uikit']);
                    $view->style('shortcodes-stylesheet', 'nxtshc/cms:assets/css/admin.css');
                }
            }, 50);
        }
    ]
];

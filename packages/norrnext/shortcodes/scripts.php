<?php
/**
 * @package    Pkb Shortcodes
 * @author     Zhukov Sergey <zom688@gmail.com>
 * @copyright  Copyright © 2016 - 2017 NorrNext. All rights reserved.
 * @license    GNU General Public License version 3 or later; see license.txt
 */
return [

    'install' => function ($app) {

        $util = $app['db']->getUtility();

        if ($util->tableExists('@norrnext_shortcodes') === false) {
            $util->createTable('@norrnext_shortcodes', function ($table) {
                $table->addColumn('id', 'integer', ['unsigned' => true, 'length' => 10, 'autoincrement' => true]);
                $table->addColumn('category_id', 'integer', ['unsigned' => true, 'length' => 10, 'notnull' => false]);
                $table->addColumn('name', 'string', ['length' => 255]);
                $table->addColumn('icon', 'string', ['length' => 255]);
                $table->addColumn('macro', 'text', ['notnull' => false]);
                $table->addColumn('permissions', 'text', ['notnull' => false]);
                $table->addColumn('html', 'text', ['notnull' => false]);
                $table->addColumn('create_date', 'datetime');
                $table->addColumn('update_date', 'datetime');
                $table->addColumn('status', 'boolean', ['default' => true]);
                $table->setPrimaryKey(['id']);
            });
        }
    },

    'uninstall' => function ($app) {

        $util = $app['db']->getUtility();

        if ($util->tableExists('@norrnext_shortcodes')) {
            $util->dropTable('@norrnext_shortcodes');
        }

        $app['config']->remove('norrnext/shortcodes');
    }

];